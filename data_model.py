"""
Contains class for the data model to be hydrated on psql
"""

import os
import io
import pandas as pd
from pandas.io.json import json_normalize
from sqlalchemy import create_engine


def retrieve_psql_database_alchemy_engine():
    """
    Uses environment variables to return engine object
    """
    return create_engine(
        'postgresql://{user_name}:{password}@{host_name}:{port}/{database}'.format(
            user_name=os.environ.get('USER_NAME'),
            password=os.environ.get('PASSWORD'),
            host_name=os.environ.get('HOSTNAME'),
            port=os.environ.get('PORT'),
            database=os.environ.get('DATABASE')))


class DataModel:
    """
    A dataframe for each table loaded to postgres
    Methods for handling merging of incoming json
    and loading of the final output
    """
    def __init__(self):
        self.users = pd.DataFrame()
        self.orders = pd.DataFrame()
        self.order_line_items = pd.DataFrame()
        # self.products = ran out of time

    def build_data_source(self, order_rows):
        """
        Calls merge functions on the list read in
        """
        self.merge_orders(order_rows)
        self.merge_users(order_rows)
        self.merge_order_line_items(order_rows)

    def merge_orders(self, order_rows):
        """
        Reads new user_ids in json and
        adds to user dataframe
        """
        mutable_data_in = pd.DataFrame(order_rows).rename(
            {'id': 'order_id'}, axis='columns')
        mutable_data_in.drop(['line_items'], axis='columns', inplace=True)
        mutable_data_in['total_price_usd'] = mutable_data_in['total_price_usd'].apply(pd.to_numeric, errors='coerce')
        mutable_data_in = mutable_data_in.infer_objects()
        mutable_data_in = mutable_data_in.set_index('order_id', verify_integrity=True)
        self.orders = pd.concat([self.orders, mutable_data_in]).drop_duplicates()

    def merge_users(self, order_rows):
        """
        Users have a user_id
        They have some aggregate metrics which are added later in the pipeline
        """
        mutable_data_in = pd.DataFrame(order_rows)
        mutable_user_data = pd.DataFrame(mutable_data_in['user_id'].unique(), columns=['user_id']).set_index('user_id')
        self.users = pd.concat([
            self.users,
            mutable_user_data], axis='columns').drop_duplicates()

    def merge_order_line_items(self, order_rows):
        """
        Line items have order_line_item_id,
        product_id, variant_id and quantity
        """
        mutable_data_in = pd.DataFrame(order_rows).rename(
            {'id': 'order_id'}, axis='columns')
        normalized_mutable_data_in = (
            pd.concat(
                {i: json_normalize(x) for i, x in mutable_data_in.pop('line_items').items()}
            )
            .reset_index(level=1, drop=True)
            .join(mutable_data_in)) # get order_id
        normalized_mutable_data_in = normalized_mutable_data_in.rename({'id': 'order_line_item_id'}, axis='columns')
        self.order_line_items = pd.concat([
            self.order_line_items,
            normalized_mutable_data_in[[
                'order_id',
                'order_line_item_id',
                'variant_id',
                'quantity',
                'product_id'
            ]].set_index('order_line_item_id')]).drop_duplicates()


    def compute_users_metrics(self):
        """
        Uses order dataframe to compute user metrics
        Aggregate metrics calculated after dataframes are built
        Sample metrics
        - Total number of orders
        - Total sum of order amounts
        TODO: More user metrics, some ideas:
        - avg time between orders (mean, median)
        - products ordered together
        """
        grouped_orders = self.orders[['user_id', 'total_price_usd']].groupby('user_id')
        count_user_orders = grouped_orders.count().rename(
            {'total_price_usd': 'total_number_of_orders'},
            axis='columns')
        sum_user_amounts = grouped_orders.sum().rename(
            {'total_price_usd': 'sum_of_ordered_amount'}, axis='columns')
        self.users = self.users.join(count_user_orders).join(sum_user_amounts)

    def rehydrate_data_to_psql(self):
        """
        Drops previous run and creates a new table
        for each of the datasets in the model
        """
        engine = retrieve_psql_database_alchemy_engine()
        for table_name, dataframe in self.__dict__.items():
            dataframe.head(0).to_sql(
                table_name,
                engine,
                if_exists='replace')
            connection = engine.raw_connection()
            cursor = connection.cursor()
            output = io.StringIO()
            dataframe.to_csv(output, sep='\t', header=False)
            output.seek(0)
            cursor.copy_from(output, table_name, null="")
            connection.commit()
            print('Completed loading {table_name}'.format(table_name=table_name))
