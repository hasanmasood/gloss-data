# gloss-data

A project to read a zip file
and upload json stored within
as rows in psql database, using:
- SqlAlchemy
- Pandas
- io

Set up the project by:
- python3 -m venv env
- source env/bin/activate
- pip install -r requirements.txt
- add credentials in set_environment_variables.sh
- source set_environment_variables.sh
- execute via python transform_and_load.py

As a bonus feature we have a CI pipeline set up to execute this as a manual job available on push to master

This was done in a short time span for purpose of demonstration, a number of open avenues for the evolution of this codebase exist:
- testing
- exception handling
- data scaling
etc.
