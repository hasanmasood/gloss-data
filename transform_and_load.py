"""
Main file that executes the data loader
"""
import json
import io
from zipfile import ZipFile
import requests
from data_model import DataModel

ZIP_URL = 'https://s3.amazonaws.com/data-eng-homework/v1/data.zip'

if __name__ == "__main__":
    RESPONSE = requests.get(ZIP_URL)
    DATA_MODEL = DataModel()
    with ZipFile(io.BytesIO(RESPONSE.content)) as data_zip:
        for json_file in data_zip.infolist():
            print(
                'Processing file: {file_name}'.format(
                    file_name=json_file.filename))
            orders_json = json.loads(
                data_zip.read(json_file).decode()
            )
            try:
                DATA_MODEL.build_data_source(orders_json['orders'])
            except KeyError:
                print(json_file.filename + ' json file has no "orders" key')
    DATA_MODEL.compute_users_metrics()
    DATA_MODEL.rehydrate_data_to_psql()
